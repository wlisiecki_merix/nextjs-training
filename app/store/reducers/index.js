import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import auth from '@reducers/auth';
import entities from '@store/reducers/entities';

export default combineReducers({
  auth,
  entities,
  form: formReducer,
});
