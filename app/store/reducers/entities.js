import merge from 'lodash/merge';
import { prefix } from '@actions/characters';

const initialState = {
  isFetching: false,
  result: {
    error: '',
  },
};

const identity = () => state => state;

const getRequest = () => state => merge({}, state, { isFetching: true });
const getRequestFinish =
action => state => merge(
  {}, state, action.payload.entities, { result: action.payload.result }, { isFetching: false }
);

function generateReducers(name) {
  return {
    [`${prefix} GET_${name}_FAILURE`]: getRequestFinish,
    [`${prefix} GET_${name}_REQUEST`]: getRequest,
    [`${prefix} GET_${name}_SUCCESS`]: getRequestFinish,
  };
}

const reducers = {
  ...generateReducers('CHARACTERS'),
  ...generateReducers('SINGLE_CHARACTER'),
};

export function selectReducer(type) {
  return reducers[type] || identity;
}

export default function (state = initialState, action) {
  return selectReducer(action.type)(action)(state);
}
