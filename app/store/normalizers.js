import {
  denormalize,
  normalize,
  schema,
} from 'normalizr';
import pick from 'lodash/pick';

const location = new schema.Entity('location', {}, { idAttribute: 'url' });
const characters = new schema.Entity('characters', { location });
const character = new schema.Entity('character', { location });

const mySchema = {
  character,
  characters: [characters],
};

export function applyDenormalization(prop) {
  return (state) => {
    const { result } = state.entities;

    return denormalize(pick(result, prop), mySchema, state.entities);
  };
}

export function applyNormalization(data) {
  return normalize(data, mySchema);
}
