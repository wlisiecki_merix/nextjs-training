import { applyDenormalization } from '../normalizers';

export const charactersSelectors = {
  getCharacter: applyDenormalization('character'),
  getCharacters: applyDenormalization('characters'),
  getError: state => state.entities.result.error,
};
