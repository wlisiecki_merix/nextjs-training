import request from '@utils/request';

export const prefix = '[CHARACTERS]';

export const GET_CHARACTERS_REQUEST = `${prefix} GET_CHARACTERS_REQUEST`;
export const GET_CHARACTERS_SUCCESS = `${prefix} GET_CHARACTERS_SUCCESS`;
export const GET_CHARACTERS_FAILURE = `${prefix} GET_CHARACTERS_FAILURE`;
export const GET_SINGLE_CHARACTER_REQUEST = `${prefix} GET_SINGLE_CHARACTER_REQUEST`;
export const GET_SINGLE_CHARACTER_SUCCESS = `${prefix} GET_SINGLE_CHARACTER_SUCCESS`;
export const GET_SINGLE_CHARACTER_FAILURE = `${prefix} GET_SINGLE_CHARACTER_FAILURE`;

const initAction = type => ({ type });
const getAction = (type, payload) => ({
  payload,
  type,
});

export const getCharacters = ({ name }) => (dispatch) => {
  dispatch(initAction(GET_CHARACTERS_REQUEST));

  return request
    .get(`character/?name=${name}`)
    .then(({ data }) => {
      dispatch(getAction(
        GET_CHARACTERS_SUCCESS,
        {
          characters: data.results,
          error: '',
        }
      ));
    }, (err) => {
      dispatch(getAction(
        GET_SINGLE_CHARACTER_FAILURE,
        {
          characters: [],
          error: err.response.statusText,
        }
      ));
    });
};

export const getSingleCharacter = ({ id }) => (dispatch) => {
  dispatch(initAction(GET_SINGLE_CHARACTER_REQUEST));

  return request
    .get(`character/${id}`)
    .then(({ data }) => {
      dispatch(getAction(
        GET_SINGLE_CHARACTER_SUCCESS,
        {
          character: data,
          error: '',
        }
      ));
    }, (err) => {
      dispatch(getAction(
        GET_SINGLE_CHARACTER_FAILURE,
        {
          character: {},
          error: err.response.statusText,
        }
      ));
    });
};
