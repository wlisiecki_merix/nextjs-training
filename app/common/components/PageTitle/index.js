import React from 'react';
import { Header } from 'semantic-ui-react';
import propTypes from 'prop-types';

export const PageTitle = ({ title }) => (
  <Header
    as="h2"
    textAlign="center"
  >
    { title }
  </Header>
);

PageTitle.propTypes = {
  title: propTypes.string.isRequired,
};
