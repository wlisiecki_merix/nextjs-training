import PropTypes from 'prop-types';

export const ItemPropTypes = PropTypes.shape({
  created: PropTypes.string,
  episode: PropTypes.arrayOf(PropTypes.string),
  gender: PropTypes.string,
  id: PropTypes.number,
  image: PropTypes.string,
  location: PropTypes.shape({
    name: PropTypes.string,
    url: PropTypes.string,
  }),
  name: PropTypes.string,
  origin: PropTypes.shape({
    name: PropTypes.string,
    url: PropTypes.string,
  }),
  species: PropTypes.string,
  status: PropTypes.string,
  type: PropTypes.string,
  url: PropTypes.string,
});
