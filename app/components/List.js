import React from 'react';
import {
  Container,
  Image,
  List,
} from 'semantic-ui-react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { charactersSelectors } from '@selectors/characters';
import { ItemPropTypes } from '../types/item.types';
import { PageTitle } from '../common/components/PageTitle';

const ListComponent = ({
  items,
  error = '',
}) => (
  <Container>
    <PageTitle title="List of characters" />
    { error ?
      <span>{error}</span> :
      (
        <List
          divided
          relaxed
          size="large"
        >
          { items.characters.map(item => (
            <Link
              href="/details/[id]"
              as={`/details/${item.id}`}
              key={item.id}
            >
              <List.Item>
                <Image
                  avatar
                  src={item.image}
                />
                <List.Content>
                  <List.Header>{ item.name }</List.Header>
                </List.Content>
              </List.Item>
            </Link>
          ))}
        </List>
      )
    }
  </Container>
);

const mapStateToProps = state => ({
  error: charactersSelectors.getError(state),
  items: charactersSelectors.getCharacters(state),
});

ListComponent.propTypes = {
  error: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(ItemPropTypes).isRequired,
};

export default connect(mapStateToProps)(ListComponent);
