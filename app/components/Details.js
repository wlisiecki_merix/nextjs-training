import React from 'react';
import {
  Card,
  Container,
  Image,
} from 'semantic-ui-react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { charactersSelectors } from '@selectors/characters';
import { ItemPropTypes } from '../types/item.types';
import { PageTitle } from '../common/components/PageTitle';

const DetailsComponent = ({
  error = '',
  item,
}) => (
  <Container>
    <PageTitle title="Character details" />
    { error ?
      <span>{error}</span> :
      (
        <Card centered>
          <Image src={item.character.image} />
          <Card.Content>
            <Card.Header>{ item.character.name }</Card.Header>
            <Card.Meta>
              <span className="date">{ item.character.species }</span>
            </Card.Meta>
            <Card.Description>{ item.character.type }</Card.Description>
            <Card.Description>{ item.character.location.name }</Card.Description>
          </Card.Content>
        </Card>
      )
    }
  </Container>
);

const mapStateToProps = state => ({
  error: charactersSelectors.getError(state),
  item: charactersSelectors.getCharacter(state),
});

DetailsComponent.propTypes = {
  error: propTypes.string.isRequired,
  item: ItemPropTypes.isRequired,
};

export default connect(mapStateToProps)(DetailsComponent);
