import React from 'react';
import {
  Button,
  Container,
  Input,
} from 'semantic-ui-react';
import Link from 'next/link';

class HomeComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchPhrase: '',
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ searchPhrase: event.target.value });
  }

  render() {
    const { searchPhrase } = this.state;

    return (
      <Container textAlign="center">
        <Input
          type="text"
          placeholder="Search"
          size="big"
          style={{
            marginRight: '10px',
          }}
          onChange={this.handleChange}
        />
        <Link
          href="/list/[search]"
          as={`/list/${searchPhrase}`}
        >
          <Button size="big">
            Search
          </Button>
        </Link>
      </Container>
    );
  }
}

export default HomeComponent;
