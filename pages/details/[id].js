import React from 'react';
import Details from 'app/components/Details';
import { getSingleCharacter } from '@actions/characters';

class DetailsPage extends React.Component {
  static async getInitialProps({
    query,
    store,
  }) {
    await getSingleCharacter({ id: query.id })(store.dispatch);

    return {};
  }

  render() {
    return (
      <Details />
    );
  }
}

export default DetailsPage;
