import React from 'react';
import List from 'app/components/List';
import { getCharacters } from '@actions/characters';

class ListPage extends React.Component {
  static async getInitialProps({
    query,
    store,
  }) {
    await getCharacters({ name: query.search })(store.dispatch);

    return {};
  }

  render() {
    return (
      <List />
    );
  }
}

export default ListPage;
